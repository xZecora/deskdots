#!/bin/bash
BG="#282828"
FG="#EBDBB2"
ORANGE="#D65D0E"
RED="#CC241D"
GREEN="#98971A"
YELLOW="#D79921"
BLUE="#83A598"
PURPLE="#B16286"
AQUA="#689D6A"

options="Cancel\nShutdown\nRestart\nSleep"
selected=$(echo -e $options | dmenu -nb "$BG" -nf "$FG" -sb "$AQUA" -sf "$FG" -shb "$YELLOW" -shf "$ORANGE" -nhf "$ORANGE" -nhb "$BG" -fn "MesloLGS NF:pixelsize=18" -l 4)
if [[ $selected = "Shutdown" ]]; then
	poweroff
elif [[ $selected = "Restart" ]]; then
	reboot
elif [[ $selected = "Sleep" ]]; then
	systemctl suspend
fi
