########################################################
########################################################
########███████╗███████╗██╗  ██╗██████╗  ██████╗########
########╚══███╔╝██╔════╝██║  ██║██╔══██╗██╔════╝########
########  ███╔╝ ███████╗███████║██████╔╝██║     ########
######## ███╔╝  ╚════██║██╔══██║██╔══██╗██║     ########
########███████╗███████║██║  ██║██║  ██║╚██████╗########
########╚══════╝╚══════╝╚═╝  ╚═╝╚═╝  ╚═╝ ╚═════╝########
########################################################
########################################################
if [ $TERM = "xterm-kitty" ] || [ $TERM = "st-256color" ]
then
    source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
    ZSH_THEME="powerlevel10k/powerlevel10k"
    [[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh
else
    autoload -U colors && colors 
    PS1="%B%{$fg[red]%}[%{$fg[yellow]%}%n%{$fg[green]%}@%{$fg[blue]%}%M %{$fg[magenta]%}%~%{$fg[red]%}]%{$reset_color%}%% %b"
fi
#pfetch

export ZSH="$HOME/.config/oh-my-zsh"
 
HYPHEN_INSENSITIVE="true"

export UPDATE_ZSH_DAYS=1
export XDG_RUNTIME_DIR=/home/vitrial/.cache/xdg
export RUNLEVEL=3
export GOPATH=/home/vitrial/go
export RUST_BACKTRACE=full
export PATH="$PATH:$HOME/.emacs.d/bin:$HOME/.local/bin"
export QT_QPA_PLATFORMTHEME=qt6ct

plugins=(git)

source $ZSH/oh-my-zsh.sh

alias zshrc="vim ~/.zshrc"
alias vimrc="vim ~/.config/nvim/init.vim"
alias emacsrc="vim ~/.doom.d/config.el"
alias :q="exit"
alias vemacs="emacs -nw"
alias vim="vim"
alias ls="exa -als type"
#alias bspwm="xinit bspwm"
#alias qtile="xinit qtile"
alias rat="startx ~/.xinitrc ratpoison"
alias dwm="startx ~/.xinitrc dwm"
alias qpwm="startx ~/.xinitrc qpwm"

HISTSIZE=10000
SAVEHIST=10000
HISTFILE=~/.cache/zsh/.zsh_history
export ZSH_COMPDUMP=$ZSH/cache/.zcompdump-$HOST

plugins=( git )

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
#[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh
